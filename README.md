# SmartToll2.0

#### Updated version of SmartToll project done as part of Certified Blockchain Architect program at Kerala Blockchain Academy.

###### Updates: New UI and smart contract optimisation for gas.

### For latest updates, use branch `develop-docker-multinode`